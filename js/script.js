(function($){
    // Test si la fonction mobileMenu existe
    if (jQuery.isFunction(jQuery.fn.mobileMenu)) {
        //Surcharge des options
        var options = {
            combine: false,
            groupPageText: '',
            nested: true,
            prependTo: '.main',
            switchWidth: 640,
            topOptionText: 'Choisir une page'
        };

        $(document).ready(function(){
            $('.menu-conteneur > ul, #nav > ul').mobileMenu(options);
        });
    }

    $(document).ready(function(){
        //Bind click sur les items de la liste d'articles
        $('.content .liste .liste-items').delegate("li", "click", function() {
            var href = $(this).find(".entry-title a").attr("href");
            if(href){
                $('body').addClass("wait");
                document.location.href = href;
            }
        });

        //Ajouter la classe submenu
        $("#nav li:has(ul), .menu-conteneur li:has(ul)").addClass("submenu");
    });
})(jQuery);